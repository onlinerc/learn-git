# Start Here

First, lets explain even what Git is!

### What

Git is a version control system. 
In layman's terms, 
it is a tool to keep track of past alternative versions of files.
This means you can check or modify old versions without having to store completely new files. 
Also, different versions of multiple different files are kept track together. 
This makes editing or storing files more like a pencil instead of fine ink that you can never erase.


### Why

Why use a tool instead of just dealing with it?
You could try to make versions of files like so:
```
folder
├── file.txt
├── file_v1.txt
├── file_v2.txt
└── old_file_v2.txt
```

But this starts to get really out of hand quickly when you have multiple files and keep making more and more files:

```
folder
├── file.txt
├── file_v1.txt
├── file_v2.txt
├── new_file_v1.txt
├── new_old_file_v2.txt
├── old_file_v2.txt
├── older_file_v2.txt
├── file_v1.txt
├── file_final_version.txt
├── file_final_final_version.txt
├── file_final_version_FOR-REAL-THIS-TIME.txt
├── file_final_version_OK_OKAY_THIS_IS_THIS_ACTUAL_FINAL.txt
├── file_final_version_DONT_LIST_TO_THE_OTHER_ONES-THIS_IS_REAL.txt
└── file_HELP_I_AM_TRAPPED_IN_AN_OFFICE_JOB_AND_I_NEED_YOU_TO_GET_ME_OUT.txt
```

Yeah.

...

That is why programmers decades ago came up with git for people to work on the same project together. 
It makes it really easy for people working together online or project involving some sort of software to collaborate with each other.

Imagine designing Android, making Windows, or creating the designs for a car without version control. 
Hundreds of files and thousands of lines of data. 

Git makes really **big** projects *possible* and really **small** projects *easier*.

### Who

> But who even uses git?
> Is it that popular?
> Will I ever see it in the industry?
> Why learn it if no one uses it?

All big tech companies use git (including Microsoft, Google, Facebook, etc). 
Most small teach companies use git too. 
Most software projects are made with git. 
Many engineering projects are backed with git (including JohnDeer and Catepillar). 
I could go on, but the truth is that (probably) over 99% of all software projects use git today.

### Where

> What about in software I have already used?

Android AND iPhone is built on git. 
Windows and macOS are built with git. 
Git was actually invented originally to develop the Linux kernel. 
Even google drive includes a slimmed down version system similar to git, 
but without a lot of the needed functionality for making projects. 
So you have probably used git at some point without even knowing it!


### How

We'll now go over the different concepts of git and how to actually use the darn thing.

Git works by storing file changes and not actual different files. 
These are stored in a hidden folder called `.git` which builds the files you view on your file system. 
