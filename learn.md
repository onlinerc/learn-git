##Git, the adventure begins to slay the *all and mighty*

![The All and Mighty Git](https://en.pimg.jp/079/427/394/1/79427394.jpg)

In git one of the most difficult tasks is understanding all the crazy terminology. Here is a bunch of gits common and more advanced terminology. Dont feel obligated to learn it all now as you can refer back to it as you're reading. A nice skim through it though couldnt hurt.

* Common Terminology
    * Commit
    * Delta
    * Diff

