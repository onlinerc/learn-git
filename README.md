# Learn Git

*an education on the concepts and workflow of git*

### Usage

All documents will be stored in the `docs` directory. 
Please resort to `docs/start-here.md` to start your journey.


### Contributions

Please start by making an issue describing the parts you want to fix, modify, or append. 
If approved in the issue, make a pull request for any contributions to the repository.


### About

Please resort to the repository's issues for bugs, grammatical errors, or other suggested improvements.

For help, please contact the authors directly.

Copyright (c) Clayton Voges 2022
